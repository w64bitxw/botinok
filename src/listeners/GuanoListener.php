<?php
require_once dirname(__FILE__)."/../vk/VkBotListener.php";
require_once dirname(__FILE__)."/util/VkMessage.php";

class GuanoListener implements VkBotListener {
	protected $guanos = [];
	
    function __construct() {
		$this->guanos = [
			"/\bкок[е|и]{1}р\b/ui" => "кокер",
			"/\bформул[а|ы|е|у]{1}\b/ui" => "формула",
			"/\bкон[а|ы|е|у]{1}\b/ui" => "кона",
			"/\bшут[е|и]р[а|ы|е|у]{0,1}\b/ui" => "шутер",
			"/\bреб[а|у]{1}\b/ui" => "реба",
			"/\bфр[и|е]ройд\b/ui" => "фреройд",
			"/\bшиман[а|у|о]{1}\b/ui" => "шимано",
			"/\bсрам[а|у]{0,1}\b/ui" => "срам",
			"/\b[к|г]{1}[о|а]{1}[м]{1,2}ен[ц|с]{1}аль\b/ui" => "коменсаль",
			"/\b(пхп|php)\b/ui" => "пхп",
			"/\b(п[и|е]тон[а|у]{0,1}|python)\b/ui" => "питон",
			"/\b([д]{0,1}жав[а|у|е|ы]{1}|java)\b/ui" => "джава",
			"/\b(дотн[н|э]{1}т|\.net)\b/ui" => "дотнет",
			"/\b([д]{0,1}жа[б|в]{1}аскрипт[а|у|е|ы]{0,1}|js|javascript)\b/ui" => "джаваскрипт",
		];
    }
	
    public function getEventType() {
		return 4; // new messages
	}
	
    public function execute(VkApi $api, Array $args) {
		$message = new VkMessage($args);
		
        $outbox = ($message->flags & 2) === 2;
        //if ($outbox) return; // do not process outgoing messages
        if (preg_match('/\b#(ботинок)\b/ui', $message->text)) return false;

		$stopPropagation = false;
		$data = ["говно", "пиздец", "хуйня", "уебанство", "ниочень", "чугуний", "пережитокпрошлого"];
        foreach ($this->guanos as $regexp => $name) {
            if (preg_match($regexp, $message->text, $args)) {
				$api->sendMessage($message->from_id, "#{$name}". $data[array_rand($data)]);
				$stopPropagation = true;
                break;
            }
        }
		return $stopPropagation;
	}
}