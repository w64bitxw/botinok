<?php
// http://api.openweathermap.org/data/2.5/weather?appid=23508dc29fb7b114831b7bd30ff912a6&q=Ижевск
require_once dirname(__FILE__)."/../vk/VkBotListener.php";
require_once dirname(__FILE__)."/util/VkMessage.php";

class WeatherListener implements VkBotListener {
    protected $curl;
	protected $config = [];

    function __construct($config) {
        $this->config = $config;

        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => false,
        ));
    }
	
    public function getEventType() {
		return 4; // new messages
	}
    
    protected function isOptimal($weather) {
        $temp = $weather["main"]["temp"]-273.15;
        $pressure = round($weather["main"]["pressure"]*0.750062);
        $wind = $weather["wind"]["speed"];
        
        $result = "";
        if ($temp >= 35) {
            $result = "Пиздец жарень";
        }
        if ($temp >= 30) {
            $result = "Жарко, блять";
        }
        if ($temp >= 25) {
            $result = "Жарковато, но сойдёт";
        }
        if ($temp <= 17) {
            $result = "Прохладно, надо приодеться!";
        }
        if ($temp > 17 && $temp < 25) {
            $result = "Оптимально для катания!";
        }
        if ($temp < 0) {
            $result = "Холодно пиздец, подумай дважды";
        }
        $temp = round($temp);
        return $result ."<br>$temp градусов, давление $pressure, ветер $wind м/с";
    }
	
	protected function current($city) {
		$city = trim($city);
		$params = $this->config;
		$params['q'] = $city?:'Izhevsk';
		$url = 'http://api.openweathermap.org/data/2.5/weather?'. http_build_query($params);
        curl_setopt_array($this->curl, [
            CURLOPT_URL => $url,
        ]);

        $response = json_decode(curl_exec($this->curl), true);
		if (!isset($response["weather"]) || !count($response["weather"])) {
			return "не нашел нихуя";
		}
		
		/*$result = $response["name"]."<br>"
				."temp ".($response["main"]["temp"]-273.15).", pressure: ".($response["main"]["pressure"]*0.750062) ."<br>"
				."wind: ".$response["wind"]["speed"]." m/s, angle: ".$response["wind"]["deg"];*/
		return $response['name'] .'<br>'. $this->isOptimal($response);
	}
	
    public function execute(VkApi $api, Array $args) {
		$message = new VkMessage($args);
		
        $outbox = ($message->flags & 2) === 2;
        //if ($outbox) return; // do not process outgoing messages
        if (preg_match('/#(ботинок)\b/ui', $message->text)) return false;

        $regexps = [
            "/\bбот[инок]{0,1}[,]{0,1} погода([\w\s]*)\b/ui" => function ($args) use ($api, $message) {
                $api->sendMessage($message->from_id, $this->current(trim($args[1])));
            }
        ];

		$stopPropagation = false;
        foreach ($regexps as $regexp => $func) {
            if (preg_match($regexp, $message->text, $args)) {
                $func($args);
				$stopPropagation = true;
                break;
            }
        }
		return $stopPropagation;
	}
}