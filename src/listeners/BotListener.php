<?php
require_once dirname(__FILE__)."/../vk/VkBotListener.php";
require_once dirname(__FILE__)."/util/VkMessage.php";

class BotListener implements VkBotListener {
	public $version = '1.2.1';
	
    public function getEventType() {
		return 4; // new messages
	}
	
    public function execute(VkApi $api, Array $args) {
		$message = new VkMessage($args);
		
        $outbox = ($message->flags & 2) === 2;
        //if ($outbox) return; // do not process outgoing messages
        if (preg_match('/#(ботинок)\b/ui', $message->text)) return false;

        $regexps = [
            "/\bбот(инок){0,1} (покажись|разденься)\b/ui" => function ($args) use ($api, $message) {
                $api->sendMessage($message->from_id, "Обнажёнка со мной здесь:<br>https://bitbucket.org/john-kolt/botinok/src");
            },
            "/\bбот(инок){0,1} версия\b/ui" => function ($args) use ($api, $message) {
                $api->sendMessage($message->from_id, "Версия {$this->version}");
            },
        ];

		$stopPropagation = false;
        foreach ($regexps as $regexp => $func) {
            if (preg_match($regexp, $message->text, $args)) {
                $func($args);
				$stopPropagation = true;
                break;
            }
        }
		return $stopPropagation;
	}
}