<?php
require_once dirname(__FILE__)."/../vk/VkBotListener.php";
require_once dirname(__FILE__)."/util/VkMessage.php";

class FaceListener implements VkBotListener {
	protected $faces = [];
	
    function __construct() {
		$this->faces = [
			'214722782' => 'Димон',
		];
    }
	
    public function getEventType() {
		return 4; // new messages
	}
	
    public function execute(VkApi $api, Array $args) {
		$message = new VkMessage($args);
		
        $outbox = ($message->flags & 2) === 2;
        //if ($outbox) return; // do not process outgoing messages
        if (preg_match('/\b#(ботинок)\b/ui', $message->text)) return false;

        if (!preg_match("/\bбот[инок]{0,1}[,]{0,1}(.+)\b/ui", $message->text, $args)) return false; 

		$stopPropagation = false;
		$data = [" - хуй", "заебал", "иди нахуй", "уебан"];
        
		$fullMessage = $api->getMessage($message->message_id);
		$face = $this->faces[$fullMessage['user_id']];
        if ($face) {
			$api->sendMessage($message->from_id, $face .' '. $data[array_rand($data)]);
            $stopPropagation = true;
        }
		return $stopPropagation;
	}
}