<?php
require_once dirname(__FILE__)."/VkApiBase.php";

class VkApi extends VkApiBase {

    public function getLongPollServer() {
        $response = parent::invokeMethod('messages.getLongPollServer', [
            'use_ssl'  => '1'
        ]);
        return $response['response'] ? new LongPoolServer($response['response']) : (var_dump($response) && null);
    }

    public function getMessage($message_id) {
        $response = parent::invokeMethod('messages.getById', [
            'message_ids'  => $message_id
        ]);
        return $response['response']['items'][0];
    }

    public function sendMessage($peer_id, $message, $forward_messages = null) {
        $result = parent::invokeMethod('messages.send', [
            'peer_id' => $peer_id,
            'message' => $message. "<br>#ботинок",
            'forward_messages' => $forward_messages
        ]);
		if (isset($result['error'])) {
			parent::invokeMethod('messages.send', [
				'peer_id' => $peer_id,
				'message' => 'VK: '. $result['error']['error_msg']. "<br>#ботинок"
			]);
		}
    }

}