<?php
require_once dirname(__FILE__)."/VkBot.php";

interface VkBotListener {
    public function getEventType();
    public function execute(VkApi $api, Array $args);
}
