<?php
// Report simple running errors
error_reporting(E_ERROR | E_WARNING | E_PARSE);
setLocale(LC_ALL, 'ru_RU');

require_once dirname(__FILE__)."/config.php";
require_once dirname(__FILE__)."/src/Botinok.php";

$shortopts  = "";
$shortopts .= "t::"; // non-required
$shortopts .= "u";   // without value

$longopts  = array(
    "token::", // non-required
    "url",     // without value, print auth url
);
$options = getopt($shortopts, $longopts);

$url = isset($options['u']) || isset($options['url']);
$token = (isset($options['t']) && $options['t']) ? $options['t'] : (isset($options['token']) ? $options['token'] : null);

if ($url) {
    die('https://oauth.vk.com/authorize?' . http_build_query(array(
        'client_id' => VK_APP_ID,
        'scope' => 'messages,offline,status,friends,photos',
        'v' => Botinok::VK_API_VERSION,
        'response_type' => 'token',
        'redirect_uri' => 'https://oauth.vk.com/blank.html'
    )));
}

$token = $token?:VK_TOKEN;

$config = [];
if (GOOGLE_SEARCH_KEY && GOOGLE_SEARCH_CX) {
	$config['googleSearch'] = [
		'key' => GOOGLE_SEARCH_KEY,
		'cx' => GOOGLE_SEARCH_CX
	];
}
if (WEATHER_APPID) {
	$config['weather'] = [
		'appid' => WEATHER_APPID
	];
}

$botinok = new Botinok($token, $config);
$botinok->listen();
